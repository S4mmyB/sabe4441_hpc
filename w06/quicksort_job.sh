#!/bin/bash

#SBATCH --nodes=1
#SBATCH --time=00:15:00
#SBATCH --qos=testing
#SBATCH --partition=shas-testing
#SBATCH --cpus-per-task=24
#SBATCH --job-name=QS_strong_scale
#SBATCH --output=QS_strong_scale.out
#SBATCH --error=QS_strong_scale.err

module purge
module load gcc

for i in {1..24};do export OMP_NUM_THREADS=$i; ./test_quicksort.exe 1000000;done
for i in {1..24};do export OMP_NUM_THREADS=$i; ./test_quicksort.exe 10000000;done
for i in {1..24};do export OMP_NUM_THREADS=$i; ./test_quicksort.exe $((1000000*$i));done
for i in {1..24};do export OMP_NUM_THREADS=$i; ./test_quicksort.exe $((10000000*$i));done
