#!/bin/bash

#SBATCH --nodes=1
#SBATCH --time=00:30:00
#SBATCH --qos=testing
#SBATCH --partition=shas-testing
#SBATCH --ntasks=4
#SBATCH --job-name=histogram-job
#SBATCH --output=hs_strong_scale.out
#SBATCH --error=hs_strong_scale.err

module purge
module load gcc

for i in {1,2,4,8,12,16,20,24}; do export OMP_NUM_THREADS=$i; ./test_histogram.exe 2 1; done
for i in {1,2,4,8,12,16,20,24}; do export OMP_NUM_THREADS=$i; ./test_histogram.exe 2 3; done
for i in {1,2,4,8,12,16,20,24}; do export OMP_NUM_THREADS=$i; ./test_histogram.exe 2 4; done

for i in {1,2,4,8,12,16,20,24}; do export OMP_NUM_THREADS=$i; ./test_histogram.exe 5 1; done
for i in {1,2,4,8,12,16,20,24}; do export OMP_NUM_THREADS=$i; ./test_histogram.exe 5 3; done
for i in {1,2,4,8,12,16,20,24}; do export OMP_NUM_THREADS=$i; ./test_histogram.exe 5 4; done

for i in {1,2,4,8,12,16,20,24}; do export OMP_NUM_THREADS=$i; ./test_histogram.exe 9 1; done
for i in {1,2,4,8,12,16,20,24}; do export OMP_NUM_THREADS=$i; ./test_histogram.exe 9 3; done
for i in {1,2,4,8,12,16,20,24}; do export OMP_NUM_THREADS=$i; ./test_histogram.exe 9 4; done

for i in {1,2,4,8,12,16,20,24}; do export OMP_NUM_THREADS=$i; ./test_histogram.exe 2 1 1; done
for i in {1,2,4,8,12,16,20,24}; do export OMP_NUM_THREADS=$i; ./test_histogram.exe 2 3 3; done
for i in {1,2,4,8,12,16,20,24}; do export OMP_NUM_THREADS=$i; ./test_histogram.exe 2 4 4; done

for i in {1,2,4,8,12,16,20,24}; do export OMP_NUM_THREADS=$i; ./test_histogram.exe 5 1 1; done
for i in {1,2,4,8,12,16,20,24}; do export OMP_NUM_THREADS=$i; ./test_histogram.exe 5 3 3; done
for i in {1,2,4,8,12,16,20,24}; do export OMP_NUM_THREADS=$i; ./test_histogram.exe 5 4 4; done

for i in {1,2,4,8,12,16,20,24}; do export OMP_NUM_THREADS=$i; ./test_histogram.exe 9 1 1; done
for i in {1,2,4,8,12,16,20,24}; do export OMP_NUM_THREADS=$i; ./test_histogram.exe 9 3 3; done
for i in {1,2,4,8,12,16,20,24}; do export OMP_NUM_THREADS=$i; ./test_histogram.exe 9 4 4; done
