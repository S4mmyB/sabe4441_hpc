#include "quicksort.h"

#define TASK_MIN 1000

void scan(int *x, size_t n){
    for(size_t i=1; i<n; ++i) { x[i]+=x[i-1]; }
}

void partition(const vec_t &x, const size_t n, const long pivot, vec_t &L, size_t &L_size, vec_t &G, size_t &G_size) {

    // CREATE ARRAYS TO DETERMINE WHICH ELEMENTS ARE GREATER / LESS THAN PIVOT --> eval is the unmodified vector
    int *e = new int[n];
    int *l = new int[n];
    int *g = new int[n];

    // DETERMINE WHERE ELEMENTS PARTITION
    #pragma omp parallel for default(none) firstprivate(e,l,g,x)
    for (size_t i=0; i<n; ++i) {
        if(x[i] < pivot) {
            l[i] = e[i] = 1;
            g[i] = 0;
        }
        else {
            l[i] = e[i] = 0;
            g[i] = 1;
        }
    }

    // SCAN TO FIND NUMBER OF ELEMENTS IN EACH PARTITION
    scan(l,n);
    scan(g,n);

    // SET SIZE OF LESS THAN / GREATER THAN PARTITIONS
    L_size = l[n-1];
    G_size = g[n-1];

    // CHECK TO MAKE SURE THERE ARE ELEMENTS IN EACH PARTITION - WORST CASE THEY ALL FALL IN ONE
    if(L_size > 0) { L.reserve(int(L_size)); }
    if(G_size > 0) { G.reserve(int(G_size)); }

    vec_t * Lp = &L;
    vec_t * Gp = &G;


    #pragma omp parallel for default(none) firstprivate(l,g,e,x,Lp,Gp)
    for (size_t i=0; i<n; ++i) {
        if (e[i]) {
            Lp->push_back(x[i]);
        }
        else {
            Gp->push_back(x[i]);
        }
    }

    assert(L.size() + G.size() == n);

    delete[] e;
    delete[] l;
    delete[] g;

    return;

}

void task_quicksort(const vec_t &x, size_t n, vec_t *y, const size_t left, const size_t right) {

    // BASE CASE
    if (n < 2) {
        if(n <= 0) { return; }
        else {
            y->at(left) = x[0];
        }
        return;
    }

    long pivot = x[n-1];
    vec_t L, G;
    size_t L_size, G_size;

    partition(x, n-1, pivot, L, L_size, G, G_size);

    y->at(left + L_size) = pivot;


    #pragma omp task default(none) firstprivate(L,L_size,y, left)
    {
        task_quicksort(L,L_size,y,left,L_size + left - 1);
    }

    #pragma omp task default(none) firstprivate(G,G_size,L_size,y, right)
    {
        task_quicksort(G,G_size,y,L_size + left + 1, right);
    }

    return;

}

void quicksort(const vec_t &x, vec_t &y) {

    size_t n = x.size();

    vec_t * yp =  &y;

    #pragma omp parallel default(none) firstprivate(x,n,yp)
    {
        #pragma omp single
        task_quicksort(x, n, yp, 0, n-1);
    }

}

/* ------ NOTES ----------

- first private differs from private in that it initializes variables to what they were outside of the parallel section rather than assigning random values
- I was getting compiler errors without default(none) - I think its because default(none) gets rid of the true default which treats parallel vars as shared
- Had to pass a pointer into the firstprivate when updating vectors. Otherwise they go back to what they were before the parallel section

  ----------------------- */
