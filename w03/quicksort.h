/*
* @input x the array to be sorted
* @output y the output array (already allocated)
*/

#include <cstddef>
#include <vector>
#include <cstdio>
#include <cassert>
#include <iostream>
#include <math.h>
#include <omp.h>

typedef std::vector<long> vec_t;

void quicksort(const vec_t &x, vec_t &y);
