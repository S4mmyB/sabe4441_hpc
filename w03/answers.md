## Assignment 3 Answers
### Sam Bennetts

#### Task 1

##### 2. Explain what the last target clean does.
Make clean removes all the already compiled object files or other files specified in the action statement. In our example the targets defined 'output' is removed as well as any other .exe and .o files which are specified using *.o and *.exe.

#### Task 2

##### 2. Modify your makefile and use variable for compiler and its flags.

```
CC=gcc
CXX=g++
CFLAG=-I.
objects=main.o message.o

output: ${objects}
	${CXX} -o output ${objects} ${CFLAG}

main.o: main.cc
	${CXX} -c -o main.o main.cc ${CFLAG}

message.o: message.cc message.h
	${CXX} -c -o message.o message.cc ${CFLAG}

clean:
	-${RM} output *.o *.exe

```

#### Task 3

##### 3. Call make clean to remove current output. Copy these files into the makefile and remove the old rules. What is printed when calling make?

```
[sabe4441@shas0136 w03]$ make
g++ -c -o main.o main.cc -I.
g++ -c -o message.o message.cc -I.
g++ -o output main.o message.o -I.
```


##### 4. Modify message.cc file and replace std::cout<<"Makefile example\n"; with std::cout<<"Makefile example modified\n";. Call make again and report the output? Explain the behavior in plain english.
The output of the newly compiled makefile was 'Makefile example modified'. When calling make only the message.o file was compiled. This is because changes were only the message.cc file. Recompiling main.o was not necessary because we did not change anything in the file. Recompile every file each time make is called would be costly, especially if the programmer had a big project structure.

#### Task 4

##### 4. In its current form, the code will not return the correct answer, modify the code by adding a single line to make it return the correct answer. Report what you added.
The OpenMP task wait directive specifies that an action should be wait until completion of child tasks generated since the beginning of the current task. Since the final result of our Fibonacci function depends on the summation of i and j, we need to ensure that both tasks have finished before we add the two numbers. In order to fix our function the following line should be added before the return statement:
```
#pragma omp taskwait
return i + j;
```

##### Task 5

###### 1. Outline your strategy for quicksort using OpenMP in pseudo-code or plain English.
**Parallel Quicksort Algorithm Pseudocode**

task_quicksort(X,Y)

	n = X.size()

	if(n < 2 and n ==1 ) {  ---> Base Case
		insert x[0] into y
		return;
	}

    pivot = x[n-1]      ---> last element will be pivot

    initialize L(Lower) array of size n-1
    initialize G(Greater) array of size n-1
	initialize E(Evaluate) array of size n-1

    for i in size(X):
        if X[i] <= p:
           L[i] = E[i] = 1
		   G[i] = 0
        else:
           G[i] = 1
		   L[i] = 0

    L_size = select(L)   ---> Sum the number of elements less than pivot
    G_size = select(G)   ---> Sum the number of elements greater than pivot

    initialize L(Left) vector of size L_size
    initialize R(Left) vector of size G_size

	#pragma omp parallel for
    for i in len(L)
		if (S[i] != 0)
			L.push(x[i])
		else
			G.push(x[i])

	y[L_size] = pivot    ---> insert pivot into final array

	#pragma omp task {
		quicksort(L, y)  ---> add recursive call to task pool to be run in parallel
	}

	#pragma omp task {
		quicksort(G, y)  ---> add recursive call to task pool to be run in parallel
	}


###### 4. Reason about the performance expectation of your code (apply the metrics we discussed in class).
![strong_scale](images/strong_scale.PNG)
![weak_scale](images/weak_scale.PNG)

The graphs above show how the quicksort algorithm performs using tasks. Evaluation using strong scaling shows us that the time it takes to run quicksort increases linearly with problem size and the number of threads. An evaluation using weak scaling indicates that increasing the number of threads for a fixed problem size decreases runtime only up to a certain point, after which adding more threads/processors does not decrease runtime.

##### Task 6

###### 6. Consider the problem of square matrix-matrix multiplication of two matrices with size nxn, where, for simplicity, n = 2<sup>k<sup> for some integer k. Consider the two PRAM cases below. Using DAGs discuss the optimal scheduling and the corresponding parallel work and depth.
![answer](images/question_6.JPG)

###### 7.1. Show how to calculate rank(x : A) in O(logn) time using O(n) operations with the EREW PRAM model.
Rank(x : A) is a count of the total number of elements in A that are less than some value x. Since we are dealing with an unsorted array A, we will have to compare x with every element in A to determine if the element is less x. A non-parallel algorithm would have to evaluate each element individually and would thus run in O(n) time. Using optimal scheduling and parallel processors we can divide the work up so that each processor gets a portion of the work, thus reducing the total amount of time it would take to determine the rank. The parallel algorithm, which is similar to the parallel histogram algorithm from week 2, can be summarized as follows:
		1. Give a copy of a zero array of size n to each of the p processors.
		2. Divide A up into p chunks and have each processor calculate the number of elements in A that are less than x. Each processor should increment A[i] if it satisfies the condition.
		3. Merge the results of the p arrays sum the result.
This algorithm still requires O(n) operations because we need to compare x to each element in the array. However, by performing the comparisons in parallel we are able to reduce the amount of time to O(logn).

###### 7.2 Suppose A is sorted, how fast can you determine the rank(x:A) with o(n) operations? Specify the PRAM model used.
Since we are determining the rank of an ordered array (remember elements of A are drawn from a linearly ordered set S) we can determine the rank by performing a modification of binary search. We start by choosing the middle element of A. If the value of the x is less than the item in the middle of the interval, narrow the interval to the lower half. Otherwise narrow it to the upper half. Repeatedly check until x is found, then use the index of x to determine how many elements are within rank(x : A). Worst case we have operations o(n) if the x is the last element in the array we will have to check all elements to determine the rank, however we will ultimately have less than n operations. The time it would take is O(logn). The model is EREW.
