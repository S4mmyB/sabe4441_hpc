/**
 * @file
 * @author Bennetts, Sam <samuel.bennetts@colorado.edu>
 * @date $Date: Tue Sep 25 2018
 *
 * @brief Tester for parallel quicksort
 */

#include "quicksort.h"
#include "omp-utils.h"
#include <cstdio>
#include <cmath>   //power
#include <cstdlib> //rand
#include <cassert>

void rand_fill(vec_t &x){

    for (auto &el : x)
        el = rand() % 1000000;
}

int main(int argc, char** argv){

    // repeatable test
    srand(2018);

    // commandline arguments
    /*if (argc<2){
        printf("Log10 of array size and number of bins are mandatory arugments for %s.\n", __FILE__);
        exit(1);
    }*/

    size_t n(atoi(argv[1]));

    vec_t x(n);
    vec_t y(n);

    rand_fill(x);

    double tic(NOW());
    quicksort(x, y);
    double toc(NOW());
    //printf("Problem Size: %d, Number Threads: %d, Elapsed time=%-5.2e\n", n, omp_get_max_threads(), toc-tic);
    printf("%-5.2e\n", toc-tic);
}
