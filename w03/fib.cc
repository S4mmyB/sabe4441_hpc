#include <iostream>
#include <stdio.h>
#include <omp.h>

using namespace std;

long fib(long n) {
  long i(0), j(0);
  if (n<2) return 1;

  #pragma omp task shared(i)
  i = fib(n-1);

  #pragma omp task shared(j)
  j = fib(n-2);

  #pragma omp taskwait
  return i + j;
}

void demo_task() {
  long n(5), v;
  #pragma omp parallel shared (n, v)
  {
    #pragma omp single /* why single? --> in order to spawn a task only once single must be used */
    printf("Final Result: %d\n", fib(n));
  }
}

int main(int argc, char * argv[]) {
  demo_task();
  return 0;
}
