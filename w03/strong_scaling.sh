#!/bin/bash

#SBATCH --nodes=1
#SBATCH --time=00:10:00
#SBATCH --qos=testing
#SBATCH --partition=shas-testing
#SBATCH --ntasks=4
#SBATCH --job-name=QS_strong_scale
#SBATCH --output=QS_strong_scale.%j.out

module purge
module load gcc

for i in {1..24} ; do
    a=$((1000000 * i));
    OMP_NUM_THREADS=$i ./test_quicksort.exe 1000000
done
