#include "poisson.h"
#include "jacobi.h"

void jacobi_solver(const MPI_Comm &comm, matvec_t mv, vec_t &diag, vec_t &rhs,
                    vec_t &u0, real_t eps_r, real_t eps_a, int k_max,
                    vec_t &u_final, int &k) {


    vec_t u_kp1, lv, u, res;
    size_t n_entries = u0.size();
    real_t res_norm = 0, res_0 = 0, condition = 0;
    u_final.resize(n_entries);
    u_kp1.resize(n_entries);
    res.resize(n_entries);
    u = u0;

    int rank;
    MPI_Comm_rank(comm, &rank);

    for(k = 0; k <= k_max; k++) {

        mv(u, lv);

        for(size_t j = 0; j < n_entries; j++) {
    		// only get residual if lv != 1, i.e. if we are not on boundary
    		if(lv[j] != -1){
    			res[j] = pow(rhs[j] - lv[j],2);
    			res_norm += res[j];
                printf("i: %d, j: %d rhs: %lf, res: %lf, lv: %lf, u: %lf \n", k, j, rhs[j], res[j], lv[j], u[j]);
    		}
    	}

        res_norm = sqrt(res_norm);

        if(k==0) {
            res_0 = res_norm;
            condition = (eps_r * fabs(res_0)) + eps_a;
        }

        if (fabs(res_norm) < condition) {
            u_final = lv;
            printf("rank: %d\n", rank);
            return;
        }

        else if (k == k_max) {
            u_final = lv;
            printf("Max iterations hit\n");
            return;
        }

        else {
            for(size_t j=0; j < u.size(); j++) {
                u_kp1[j] = (1.0 / diag[j]) * (res[j] + u[j]*diag[j]) ;
            }
            u = u_kp1;
        }
    }
}
