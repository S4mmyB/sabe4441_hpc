#include "poisson.h"
#include "jacobi.h"
#include <math.h>


int main(int argc, char **argv)
{
	// Init MPI
	MPI_Init(&argc, &argv);

	// Parse command line
	if (argc < 5) {
		printf("Incorrect number of inputs. Problem size (N), max iterations, eps_r, and eps_a are required\n");
		exit(1);
	}
	int n = atoi(argv[1]);
    int k_max = atoi(argv[2]);
	real_t eps_r = atof(argv[3]);
	real_t eps_a = atof(argv[4]);

    // Create the grid communicator and communication structures
	MPI_Comm grid_comm;
	grid_t x;
	poisson_setup(MPI_COMM_WORLD, n, grid_comm, x);

	printf("X.size = %d\n", x.size());

    // Declare variables needed for the jacobi jacobi_solver
    vec_t a, u0, rhs, diag, u_final;
    int k = 0;
    matvec_t mv = std::bind(poisson_matvec, std::ref(grid_comm), n, std::ref(a),
                            std::placeholders::_1, std::placeholders::_2);

    // Fill function f with known values
    for(int i=0; i < x.size(); i++) {
        real_t xval = sin(2 * M_PI * x[i].x);
        real_t yval = sin(12 * M_PI * x[i].y);
        real_t zval = sin(8 * M_PI * x[i].z);
        rhs.push_back(xval * yval * zval);
        a.push_back(12);
        u0.push_back(0.0);
        diag.push_back(-6.0);
    }

    // Call the jacobi function
    jacobi_solver(grid_comm, mv, diag, rhs, u0, eps_r, eps_a, k_max, u_final, k);

    // Print the results
    int rank;
    MPI_Comm_rank(grid_comm, &rank);
    if(rank == 0) {
        printf("Problem of size: %d took %d iterations\n", n, k);
    }

	MPI_Finalize();

	return 0;
}
