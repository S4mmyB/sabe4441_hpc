#!/bin/bash

#SBATCH --time=0:5:00     # walltime, abbreviated by -t
#SBATCH --nodes=3          # number of cluster nodes, abbreviated by -N
#SBATCH -o results.out     # name of the stdout redirection file, using the job number (%j)
#SBATCH -e results.err     # name of the stderr redirection file
#SBATCH --ntasks=64        # number of parallel process
#SBATCH --qos debug        # quality of service/queue (See QOS section on CU RC guide)

# run the program
echo "Running on $(hostname --fqdn)"
module load intel

eps_r=0.00001
eps_a=0.000000000001
k_max=10000

echo Begin set n = {256,512,758,1024} for residual analysis
for i in {256};
do
	mpirun -n 64 ./test_jacobi.exe $i $k_max $eps_r $eps_a
done
