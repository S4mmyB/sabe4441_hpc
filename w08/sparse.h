/*
 * @file sparse.h
 * @author Rahimian, Sam Bennetts <samuel.bennetts@colorado.edu>
 * @date $Date: Monday, 29 October 2018
 * @brief test driver for Sparse
 */

#include <vector>
#include <mpi.h>
#include <algorithm> // sort and generate
#include <cstdio>
#include <stdlib.h>
#include <cassert>
#include <math.h>

typedef double data_t;

/*
 *  Returns the shuffling index set such that v[idx] is sorted
 */
void sort_index(const std::vector<size_t> &v, std::vector<size_t> &idx);

class sparse {
  public:
    enum storage_type {CSR, COO};

    std::vector<data_t> data;
    std::vector<size_t> column;
    std::vector<size_t> row;
    size_t m,n;
    storage_type type;

    sparse(size_t m, size_t n, storage_type t = CSR):
        m(m), n(n), type(t) {}

    void transpose(sparse &B) const; /* returns transpose in B */
    void toCSR(sparse &B) const;
};
