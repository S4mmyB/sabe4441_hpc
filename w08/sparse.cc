/*
 * @file sparse.cc
 * @author Rahimian, Sam Bennetts <samuel.bennetts@colorado.edu>
 * @date $Date: Monday, 29 October 2018
 * @brief test driver for Sparse
 */

#include "sparse.h"

void sort_index(const std::vector<size_t> &v, std::vector<size_t> &idx){
    size_t n(0);
    idx.resize(v.size());
    std::generate(idx.begin(), idx.end(), [&]{ return n++; });
    std::sort(idx.begin(), idx.end(),
              [&](size_t ii, size_t jj) { return v[ii] < v[jj]; } );
}

void sparse::transpose(sparse &B) const {
    int rank = 0, nproc = 0;
    MPI_Comm_size(MPI_COMM_WORLD, &nproc);
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);

    /* Set dimensions for the CRS arrays */
    B.data.resize(data.size());
    B.column.resize(column.size());
    B.row.resize(n + 1);

    /* Sort data by columns of A --> needs to be sorted by colunms for CRS format
     * Remember that row_ptr points to first element in a column, hence the sorting
     */
    std::vector<size_t> sorted_data;
    sort_index(column, sorted_data);

    /* Copy sorted data into B */
    for(size_t i = 0; i < data.size(); i++) {
        B.data[i] = data[sorted_data[i]];
        B.column[i] = row[i];
    }

    /* Count the number of elements in each column */
    std::vector<size_t> column_count;
    for(size_t i = 0; i < n; i++) {
        column_count.push_back(0);
    }
    for(size_t i = 0; i < column.size(); i++) {
        column_count[column[i]] += 1;
    }

    /* Calculate row_ptr for B */
    B.row[0] = 0;
    for(size_t i = 0; i < B.row.size() - 1; i ++) {
        B.row[i + 1] = B.row[i] + column_count[i];
    }
    B.row[B.row.size() - 1] = B.row[0] + B.data.size();

    for(size_t i = 0; i < B.data.size(); i++) {
        printf("Rank: %d, B[%d]: %.0f, Column: %d\n", rank, i, B.data[i], B.column[i]);
        //printf("Rank: %d, B[%d]: %.0f, Column: %d, Row: %d\n", rank, i, B.data[i], B.column[i], B.row[i]);
    }

}
