/*
 * @file test_sparse.cc
 * @author Rahimian, Sam Bennetts <samuel.bennetts@colorado.edu>
 * @date $Date: Monday, 29 October 2018
 * @brief test driver for Sparse
 */

#include "sparse.h"
#include <iostream>

int main(int argc, char **argv) {

    /* Check Command Line Arguments */
    if (argc < 3) {
        printf("Input dimensions for sub-maxticies A are required ((m / nproc) x n). \n", __FILE__);
        exit(1);
    }

    /* Get MPI Info */
    MPI_Init(&argc, &argv);
    int rank = 0, nproc = 0;
    MPI_Comm_size(MPI_COMM_WORLD, &nproc);
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);

    /* Create and fill local sparse matricies */
    sparse A{local_height, local_width};
    size_t p = 4; // p is an integer such that 1/p is the probability of getting a nonzero entry.
    srand(time(NULL) + rank);
    bool empty_col = true;
    for ( size_t ii = 0; ii < local_width; ii++) {
        for ( size_t jj = 0; jj < local_height; jj++) {
            if ( 0 == rand() %  p) {
                A.data.push_back(rand() % 100);
                A.row.push_back(jj);
                A.column.push_back(ii);
                empty_col = false;
            }
            /* Ensure there are no empty columns (helps when creating the row_ptr array in CRS) */
            if ((jj == local_height - 1) && empty_col) {
                A.data.push_back(rand() % 100);
                A.row.push_back(jj);
                A.column.push_back(ii);
                empty_col = false;
            }
        }
        empty_col = true;
    }
    printf("Non-zero elements = %d\n", A.data.size());
    for(size_t i =0; i < A.data.size(); i++) {
        printf("Rank: %d, Data: %.0f, Column: %d, Row: %d\n", rank, A.data[i],
                    A.column[i], A.row[i]);
    }

    /* Transpose the local matrix */
    sparse B{local_width, local_height, sparse::COO};
    A.transpose(B);


    // the root process first aggregates the size of incoming date
    // create arrays to contain size and offset information
    size_t local_size = B.data.size();
    size_t row_size = B.row.size();
    int *local_sizes(NULL), *offsets(NULL), *row_sizes(NULL), *row_offsets(NULL);
    if (rank == 0) {
        local_sizes = new int[nproc];
        row_sizes   = new int[nproc];
        offsets     = new int[nproc];
        row_offsets = new int[nproc];
    }

    /* Gather information about size of local CSR arrays */
    MPI_Gather(&local_size, 1, MPI_INT, local_sizes, 1, MPI_INT, 0, MPI_COMM_WORLD);
    MPI_Gather(&row_size, 1, MPI_INT, row_sizes, 1, MPI_INT, 0, MPI_COMM_WORLD);

    int bufsz, bufsz2;
    data_t *global_data(NULL);
    size_t *global_column(NULL);
    size_t *global_row(NULL);

    /* Construct offset arrays to recieve data */
    if (rank == 0) {
        offsets[0] = 0;
        row_offsets[0] = 0;
        for (int ii=1; ii<nproc; ++ii) {
            offsets[ii]     = offsets[ii-1]+local_sizes[ii-1];
            row_offsets[ii] = row_offsets[ii-1]+row_sizes[ii-1];
        }
        bufsz  = offsets[nproc-1]+local_sizes[nproc-1];
        bufsz2 = row_offsets[nproc-1]+row_sizes[nproc-1];
        global_data   = new data_t[bufsz];
        global_column = new size_t[bufsz];
        global_row    = new size_t[bufsz2];
    }

    /* Gather Data */
    MPI_Gatherv(&B.data[0],local_size, MPI_DOUBLE,
                 global_data,local_sizes,offsets,MPI_DOUBLE, 0, MPI_COMM_WORLD);

    /* Gather Columns */
    MPI_Gatherv(&B.column[0],local_size, MPI_UNSIGNED_LONG_LONG,
                 global_column,local_sizes,offsets,MPI_UNSIGNED_LONG_LONG, 0, MPI_COMM_WORLD);

    /* Gather Rows */
     MPI_Gatherv(&B.row[0],local_size, MPI_UNSIGNED_LONG_LONG,
                 global_row,row_sizes,row_offsets, MPI_UNSIGNED_LONG_LONG, 0, MPI_COMM_WORLD);

    if (rank == 0) {
        for (int ii=0; ii<bufsz; ++ii)
            printf("Data[%d]: %.0f, Column[%d]: %ld, Row[%d]: %ld\n",ii,global_data[ii],ii,global_column[ii],ii,global_row[ii]);
    }

    delete[] local_sizes;
    delete[] offsets;
    delete[] global_data;
    delete[] global_column;
    delete[] global_row;

    if(rank == 0) {
        printf("\b]\n----------------------------\n");
        printf("----- Transpoze Matrix -----\n");
        printf("----------------------------\n");

        printf("B.data: [");
        for(size_t i = 0; i < B.data.size(); i++) {
            printf("%.0f ", B.data[i]);
        }
        printf("\b]\nB.column: [");
        for(size_t i = 0; i < B.column.size(); i++) {
            printf("%d ", B.column[i]);
        }
        printf("\b]\nB.row: [");
        for(size_t i = 0; i < B.row.size(); i++) {
            printf("%d ", B.row[i]);
        }
        printf("\b]\n");
    }

    MPI_Finalize();

    return 0;
}
