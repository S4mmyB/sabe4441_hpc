/**
 * @file
 * @author Rahimian, Abtin <arahimian@acm.org>
 * @revision $Revision: 25 $
 * @tags $Tags: tip $
 * @date $Date: Thu Jan 01 00:00:00 1970 +0000 $
 */

#include "poisson.h"
#include <iostream>

void poisson_setup(const MPI_Comm &comm, int n, MPI_Comm &grid_comm, grid_t &x)
{
    // Calculate number of processors
    int nproc = 0;
    MPI_Comm_size(comm, &nproc);

    // Create the division of processors in a cartesian grid
    int dims[3] = {0, 0, 0};
    if (MPI_Dims_create(nproc, 3, dims) != MPI_SUCCESS) {
        printf("Error using MPI_Dims_create.\n");
        return;
    }

    // Create the cartesian grid --> periodic grids have wrap around connections, 0 is true --> reorder = 1 allows procs to have new ranks assigned to them
    int periods[3] = {0, 0, 0};
    if (MPI_Cart_create(comm, 3, dims, periods, 1, &grid_comm) != MPI_SUCCESS) {
        printf("Error creating cartesian grid.\n");
        return;
    }

    // Find the rank and coordinates for each processor
    int rank;
    int coords[3] = {0, 0, 0};
    if (MPI_Comm_rank(grid_comm, &rank) != MPI_SUCCESS) {
        printf("Error determining rank\n");
        return;
    }
    if (MPI_Cart_coords(grid_comm, rank, 3, coords) != MPI_SUCCESS) {
        printf("Error extracting coordinates for processor %d.\n", rank);
        return;
    }

    // Calculate Sample Points --> h = 1/(n-1), x = (ih, jh, kh) --> since we are using a scaler, h, we need to modify indicies accordingly
    // The boundaries for the sample points should be calculated using the dimensions because we are not dealing with a perfect cube
    real_t H = 1.0 / (n - 1);
    real_t node_count[3] = { real_t(n / dims[0]), real_t(n / dims[1]), real_t(n / dims[2]) };
    real_t starting_index[3] = {H * coords[0] * dims[0], H * coords[1] * dims[1], H * coords[1] * dims[1] };

    for (real_t i = 0; i < node_count[0]; ++i) {
        real_t x_value = starting_index[0] + i*H;
        for (real_t j = 0; j < node_count[1]; ++j) {
            real_t y_value = starting_index[1] + j*H;
            for (real_t k = 0; k < node_count[2]; ++k) {
                real_t z_value = starting_index[2] + k*H;
                point_t point;
                point.x = x_value;
                point.y = y_value;
                point.z = z_value;
                x.push_back(point);
            }
        }
    }
    //printf("dims: [%d, %d, %d,], num_points: %d\n", dims[0], dims[1], dims[2], x.size());
}

void poisson_matvec(const MPI_Comm &grid_comm, int n, const vec_t &a, const vec_t &v, vec_t &lv)
{
    // Get info about the processor
    int rank, numProcs = 0;
    int dims[] = { 0, 0, 0 },
        periods[] = { 0, 0, 0 },
        coords[] = { 0, 0, 0 };
    MPI_Comm_rank(grid_comm, &rank);
    MPI_Comm_size(grid_comm, &numProcs);
    MPI_Cart_get(grid_comm, 3, dims, periods, coords);

    // Get the number of nodes on each axis
    real_t node_count[3] = { real_t(n / dims[0]), real_t(n / dims[1]), real_t(n / dims[2]) };
    const size_t XMAX = node_count[0] - 1;
    const size_t YMAX = node_count[1] - 1;
    const size_t ZMAX = node_count[2] - 1;

    // Lambda function to help convert sample point coordinates into index values
    const auto getIndex = [node_count] (real_t x, real_t y, real_t z) -> real_t {
        return (x * node_count[1] * node_count[2]) + (y * node_count[1]) + z;
    };


    // Get the nearby procs {-x, +x, -y, +y, -z, +z} --> Boundary if shift returns MPI_PROC_NULL
    const auto extract_values = [&a, &v, getIndex, grid_comm, XMAX, YMAX, ZMAX]
                        (real_t x, real_t y, real_t z, real_t *vnear, real_t v_this) -> void {
        bool edge = false;
        int srank;
        MPI_Status st;

        // Get the nearby procs {-x, +x, -y, +y, -z, +z} --> Boundary if shift returns MPI_PROC_NULL
        int nearProc[] =
            { MPI_PROC_NULL, MPI_PROC_NULL, MPI_PROC_NULL, MPI_PROC_NULL, MPI_PROC_NULL, MPI_PROC_NULL };
        MPI_Cart_shift(grid_comm, 0, -1, &srank, nearProc + 0);
        MPI_Cart_shift(grid_comm, 0, +1, &srank, nearProc + 1);
        MPI_Cart_shift(grid_comm, 1, -1, &srank, nearProc + 2);
        MPI_Cart_shift(grid_comm, 1, +1, &srank, nearProc + 3);
        MPI_Cart_shift(grid_comm, 2, -1, &srank, nearProc + 4);
        MPI_Cart_shift(grid_comm, 2, +1, &srank, nearProc + 5);

        /*for(real_t i=0; i < 6; i++) {
            if(nearProc[i] == MPI_PROC_NULL) {
                edge = true;
                break;
            }
        }*/

        if(x == XMAX) {
            MPI_Send(0, 1, MPI_DOUBLE, nearProc[0], 0, grid_comm);
            MPI_Recv(&vnear[0], 1, MPI_DOUBLE, nearProc[0], 0, grid_comm, &st);
        }
        else if (x == 0) {
            MPI_Send(0, 1, MPI_DOUBLE, nearProc[1], 0, grid_comm);
            MPI_Recv(&vnear[1], 1, MPI_DOUBLE, nearProc[1], 0, grid_comm, &st);
        }
        else {
            MPI_Send(&v_this, 1, MPI_DOUBLE, nearProc[0], 0, grid_comm);
            MPI_Send(&v_this, 1, MPI_DOUBLE, nearProc[1], 0, grid_comm);
            MPI_Recv(&vnear[0], 1, MPI_DOUBLE, nearProc[0], 0, grid_comm, &st);
            MPI_Recv(&vnear[1], 1, MPI_DOUBLE, nearProc[1], 0, grid_comm, &st);
        }

        if(y == YMAX) {
            MPI_Send(0, 1, MPI_DOUBLE, nearProc[2], 0, grid_comm);
            MPI_Recv(&vnear[2], 1, MPI_DOUBLE, nearProc[2], 0, grid_comm, &st);
        }
        else if (y == 0) {
            MPI_Send(0, 1, MPI_DOUBLE, nearProc[3], 0, grid_comm);
            MPI_Recv(&vnear[3], 1, MPI_DOUBLE, nearProc[3], 0, grid_comm, &st);
        }
        else {
            MPI_Send(&v_this, 1, MPI_DOUBLE, nearProc[2], 0, grid_comm);
            MPI_Send(&v_this, 1, MPI_DOUBLE, nearProc[3], 0, grid_comm);
            MPI_Recv(&vnear[2], 1, M PI_DOUBLE, nearProc[2], 0, grid_comm, &st);
            MPI_Recv(&vnear[3], 1, MPI_DOUBLE, nearProc[3], 0, grid_comm, &st);
        }

        if(z == ZMAX) {
            MPI_Send(0, 1, MPI_DOUBLE, nearProc[4], 0, grid_comm);
            MPI_Recv(&vnear[4], 1, MPI_DOUBLE, nearProc[4], 0, grid_comm, &st);
        }
        else if (z == 0) {
            MPI_Send(0, 1, MPI_DOUBLE, nearProc[5], 0, grid_comm);
            MPI_Recv(&vnear[5], 1, MPI_DOUBLE, nearProc[5], 0, grid_comm, &st);
        }
        else {
            MPI_Send(&v_this, 1, MPI_DOUBLE, nearProc[4], 0, grid_comm);
            MPI_Send(&v_this, 1, MPI_DOUBLE, nearProc[5], 0, grid_comm);
            MPI_Recv(&vnear[4], 1, MPI_DOUBLE, nearProc[4], 0, grid_comm, &st);
            MPI_Recv(&vnear[5], 1, MPI_DOUBLE, nearProc[5], 0, grid_comm, &st);
        }
    };

    real_t H = 1.0/(n - 1);
    real_t H2 = H*H;
    real_t vnear[] = { 0, 0, 0, 0, 0, 0 };
    for(real_t i=0; i < node_count[0]; ++i) {
        for(real_t j=0; j < node_count[1]; ++j) {
            for(real_t k=0; k < node_count[2]; ++k) {
                size_t index = getIndex(i, j, k);
                extract_values(i, j, k, vnear, v[index]);
                real_t lu = a[index] * v[index] -
                    ((vnear[0] + vnear[1] + vnear[2] + vnear[3] + vnear[4] + vnear[5] + (6*v[index])) / H2);
                lv.push_back(lu);
            }
        }
    }
}

void residual(const MPI_Comm &comm, matvec_t &mv, const vec_t &v, const vec_t &rhs,
              vec_t &res, real_t &res_norm)
{
    vec_t lv;
    mv(v, lv);

    for(real_t i=0; i < lv.size(); ++i) {
        printf(" %lf ", lv[i]);
        //res.push_back(std::pow(rhs[i] - lv[i], 2));
    }
    printf("\n");

    // send Left


}
