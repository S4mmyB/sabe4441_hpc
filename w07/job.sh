#!/bin/bash

#SBATCH --time=0:05:00
#SBATCH -o result-%j.out
#SBATCH -e result-%j.err
#SBATCH --qos debug

# advise task manager that maximum of 4
# tasks/processes may be spawned
#SBATCH --ntasks 4

# run the program
module purge
module load gcc
module load impi

export CC=mpicc
export CXX=mpicxx
export OMP_NUM_THREADS=4

mpirun -n 4 ./systolic-matvec.exe array.txt
