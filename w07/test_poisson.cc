/**
 * @file
 * @author Rahimian, Abtin <arahimian@acm.org>
 * @revision $Revision: 25 $
 * @tags $Tags: tip $
 * @date $Date: Thu Jan 01 00:00:00 1970 +0000 $
 *
 * @brief test driver for Possion
 */

#include "poisson.h"
#include <iostream>

void function_fill(grid_t &x, std::vector<real_t> &a, std::vector<real_t> &f, std::vector<real_t> &v) {
    real_t scalar = 12 + (312 * M_PI * M_PI) ;
    std::vector<point_t>::iterator it;
    for(it = x.begin(); it != x.end(); it++) {
        real_t value = sin(4 * M_PI * it->x) * sin(10 * M_PI * it->y) * sin(14 * M_PI * it->z);
        f.push_back(scalar * value);
        v.push_back(value);
        a.push_back(12);
        //printf("point: [%lf, %lf, %lf], v: %lf, f: %lf\n", it->x, it->y, it->z, value, scalar * value);
    }
}

int main(int argc, char** argv){

    MPI_Init(&argc,&argv);
    MPI_Comm grid_comm;
    int n(atoi(argv[1]));
    grid_t x;

    poisson_setup(MPI_COMM_WORLD, n, grid_comm, x);


    /** fill a and f based on x **/
    vec_t a, f, v;

    function_fill(x, a, f, v);

    // make a matvec object to pass to the residual function (residual
    // doesn't care how you do the matvec, it just passes an input and
    // expect and output.

    matvec_t mv = std::bind(poisson_matvec, std::ref(grid_comm), n, std::ref(a),
                            std::placeholders::_1, std::placeholders::_2);

    // now you can call mv(v,lv)
    vec_t res;
    real_t res_norm;
    residual(grid_comm, mv, v, f, res, res_norm);
    //std::cout<<"Residual norm: "<<res_norm<<std::endl;*/

    MPI_Finalize();

    /** cleanup **/
    return 0;
}
