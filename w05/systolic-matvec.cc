/**
 * @file
 * @author Sam, Bennetts <samuel.bennetts@colorado.edu>
 * @date 3 October 2018
 */

#include <iostream>
#include <vector>
#include <fstream>
#include <sstream>
#include <mpi.h>
#include "slurp_file.h"

int count_lines(const char* fname) {
    std::ifstream fh(fname, std::ios::in);

    if(!fh.good()){
        std::cerr<<"Cannot open file "<<fname<<std::endl;
        exit(0);
    }

    std::string line;
    int n(0);

    while (std::getline(fh, line))
        n++;

    fh.close();
    return n;
}

int main(int argc, char** argv){

    if (argc<2){
        std::cerr<<"File name is a required argument"<<std::endl;
        return 1;
    }

    int n = count_lines(argv[1]) - 1;

    MPI_Init(&argc,&argv);
    int rank,nproc,k;
    MPI_Comm_size(MPI_COMM_WORLD, &nproc);
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);


    std::vector<data_t> x;
    slurp_file_line(argv[1], n, x);

    for(int q = n / nproc; q > -1; q--) {
        k = rank+nproc*q;
        if (k < n) {
            std::vector <data_t> column;
            slurp_file_line(argv[1], k, column);

            std::vector <data_t> local_sum(n);
            std::vector <data_t> final_vector(n);
            MPI_Status st;

            for(int i=0; i<n; i++){
                local_sum[i] = column[i]*x[k];
            }

            if (k == n-1) {
                if (rank == 0) {
                    MPI_Send(&local_sum[0], n, MPI_INT, nproc - 1, 0, MPI_COMM_WORLD);
                } else {
                    MPI_Send(&local_sum[0], n, MPI_INT, rank - 1, 0, MPI_COMM_WORLD);
                }
            }

            if (k < n-1) {
                if(rank == nproc - 1) {
                    MPI_Recv(&final_vector[0], n, MPI_INT, 0, 0, MPI_COMM_WORLD, &st);
                } else {
                    MPI_Recv(&final_vector[0], n, MPI_INT, rank + 1, 0, MPI_COMM_WORLD, &st);
                }

                for(int i=0; i<n; i++){
                    final_vector[i] += local_sum[i];
                }

                if (k != 0) {
                    if (rank == 0) {
                        MPI_Send(&final_vector[0], n, MPI_INT, nproc - 1, 0, MPI_COMM_WORLD);
                    } else {
                        MPI_Send(&final_vector[0], n, MPI_INT, rank - 1, 0, MPI_COMM_WORLD);
                    }
                } else {
                    printf("Final Vector: [");
                    for(int i=0; i<n; i++){
                        printf("%d ", final_vector[i]);
                    }
                    printf("]\n");
                }
            }
        }
    }

/*
    while(rank+nproc*k < n) {
       std::vector <data_t> column;
       slurp_file_line(argv[1], rank+nproc*k, column);

       std::vector <data_t> local_sum(n);
       std::vector <data_t> final_vector(n);
       MPI_Status st;

       for(int i=0; i<n; i++){
           local_sum[i] = column.at(i)*x.at(rank+nproc*k);
       }


       printf("Process %d, Line: %d, local_total: [", rank, rank+nproc*k);
       for(int i(0); i<local_sum.size(); i++)
            printf("%d ", local_sum[i]);
       printf("]\n");
*/

    MPI_Finalize();
}
