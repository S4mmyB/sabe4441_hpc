/**
 * @file
 * @author Sam, Bennetts <samuel.bennetts@colorado.edu>
 * @date 3 October 2018
 */

#include <iostream>
#include <vector>
#include <fstream>
#include <sstream>
#include <mpi.h>
#include "slurp_file.h"

int main(int argc, char** argv){

    if (argc<2){
        std::cerr<<"File name is a required argument"<<std::endl;
        return 1;
    }

    MPI_Init(&argc,&argv);
    int rank,nproc;
    MPI_Comm_size(MPI_COMM_WORLD, &nproc);
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);

    std::vector<data_t> data;
    slurp_file_line(argv[1] /* file name */, rank /* line number */, data);

    data_t mx(data[0]);
    for (int i(0);i<data.size();++i)
        if (data[i]>mx) mx=data[i];

    std::cout<<"[rank "<<rank<<"] "<<mx<<std::endl;

    MPI_Finalize();
}
