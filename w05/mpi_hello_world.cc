/**
 * @file
 * @author Sam, Bennetts <samuel.bennetts@colorado.edu>
 * @date 3 October 2018
 */

#include <mpi.h>
#include <iostream>
#include <stdio.h>

int main(int argc, char** argv){

    MPI_Init(&argc,&argv);
    int rank,nproc;
    MPI_Comm_size(MPI_COMM_WORLD, &nproc);
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);

    std::cout<<"Hello from processor "<<rank<<" of "<<nproc<<std::endl;

    MPI_Finalize();
}
