#include "histogram.h"



void histogram(const vec_t &x, int num_bins, vec_t &bin_bdry, count_t &bin_count) {

    double mn = std::numeric_limits<double>::max();
    double mx = std::numeric_limits<double>::min();

    #pragma omp parallel for reduction(max : mx) reduction(min : mn)
    for(int i=0; i < x.size(); i++) {
        if(x[i] > mx) { mx = x[i]; }
        if(x[i] < mn) { mn = x[i]; }
    }

    double bin_size = (mx - mn) / num_bins;

    bin_bdry = vec_t(num_bins+1);
    bin_bdry[0]=mn*(1-1E-14);
    bin_bdry[num_bins]=mx*(1+1E-14);

    for(int i=1; i < num_bins; i++) {
        bin_bdry[i] = bin_bdry[i-1] + bin_size;
    }

    bin_count = count_t(num_bins);

    int num_threads = omp_get_max_threads();
    int local_histogram[num_threads*num_bins] = {0};

    #pragma omp parallel
    {
        int tid = omp_get_thread_num();
        int bin;
        int index;

        #pragma omp for
        for (int i = 0; i < x.size(); i++) {
            bin = ((x[i] - mn) / bin_size) - 1;
            if(bin < 0) { bin = 0; }
            index = tid*num_bins + bin;
            local_histogram[index] += 1;
        }

        #pragma omp for
        for(int i=0; i<num_bins; i++) {
            for(int j=0; j<num_threads; j++) {
                index = j*num_bins + i;
                bin_count[i] += local_histogram[j*num_bins + i];
            }
        }
    }
}
