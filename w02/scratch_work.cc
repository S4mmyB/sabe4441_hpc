/**
 * @file
 * @author Rahimian, Abtin <arahimian@acm.org>
 * @revision $Revision: 3 $
 * @tags $Tags:  $
 * @date $Date: Tue Sep 26 23:15:22 2017 -0600 $
 *
 * @brief Tester for histogram
 */

#include "histogram.h"
#include "omp-utils.h"
#include <cstdio>
#include <cmath>   //power
#include <cstdlib> //rand
#include <cassert>

void rand_fill(vec_t &x){
    for (auto &el : x)
        el = 1e-6*(rand() % 1000000); /* between 0 and 1 */
}

int main(int argc, char** argv){
    size_t n(std::pow(4,6));
    size_t num_bins(std::pow(2,2));

    vec_t x(n), bin_bdry;
    count_t bin_count;

    rand_fill(x);

    int nt = omp_get_max_threads();
    printf("Histogram of a random array of size %d, num_bins=%d, num_threads=%d\n",n,num_bins,nt);
    //printf("Bin boundry size: %d\n", bin_bdry.size());

    histogram(x, num_bins, bin_bdry, bin_count);

    return 0;
}
