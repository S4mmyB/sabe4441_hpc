#!/bin/bash

#SBATCH --nodes=1
#SBATCH --time=00:02:00
#SBATCH --qos=testing
#SBATCH --partition=shas-testing
#SBATCH --ntasks=4
#SBATCH --job-name=histogram-job
#SBATCH --output=histogram-job.%j.out

module purge
module load gcc

./test_histogram.exe 2 1
./test_histogram.exe 5 3
./test_histogram.exe 9 4
./test_histogram.exe 2 1 3
./test_histogram.exe 5 3 3
./test_histogram.exe 9 4 3
