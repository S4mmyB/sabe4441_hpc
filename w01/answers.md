# Assignment 1 Answers
## Sam Bennetts

### Task 1

#### 1. What does the man page for bash says for PATH? Use man bash to print the man page.
The search for path The  search  path  for  commands.  It is a colon-separated list of directories in which the shell looks for commands (see COMMAND EXECUTION below). A zero-length (null) directory name in the value of PATH indicates the current directory. A null directory name may appear as two adjacent  colons, or as an initial or trailing colon.  The default path is system-dependent, and is set by the administrator who installs bash. A common value is /usr/gnu/bin:/usr/local/bin:/usr/ucb:/bin:/usr/bin.

#### 2. What is the entry for LD_LIBRARAY_PATH in the man page for ld (linker on Linux)?
In summary of what was specified on the man page for ld (the linker on Linux)... When using ELF or SUNOS, one shared library may require another. This happens when an ld -shared link includes a shared library as one of the input files. The -rpath-link option, if used, may specify a sequence of directory names either by specifying a list of names separated by colons, or by appearing multiple times. For a native linker, the linker would search the contents of the environment variable LD_LIBRARY_PATH to locate required shared libraries.

#### 3. What is the value stored in HOME. You can use echo to show the value of a variable.
/home/sabe4441/

### Task 2

#### 5. Inspect the details of the default intel module on Summit. How does it change the PATH and LD_LIBRARY_PATH variables?
The PATH variable first becomes: /curc/sw/gcc/5.4.0/lib64, then /curc/sw/intel/17.4/compilers_and_libraries_2017.4.196/linux/compiler/lib/intel64
The LD_LIBRARY_PATH variable first becomes: /curc/sw/gcc/5.4.0/lib64, then /curc/sw/intel/17.4/compilers_and_libraries_2017.4.196/linux/compiler/lib/intel64

These variables are modified using the prepend_path function.  

#### 6. After you load the intel module, how does the list of available modules change?
Two additional sections become available: MPI Implementations and Compiler Dependent Applications.

#### 7. Describe the motivation for hierarchical module system
Libraries built with one compiler need to be linked with applications built with the same compiler version. This can be done using a Message Passing Interface (MPI). Parallel libraries and applications must be built with a matching MPI library and compiler. To avoid loading an incompatible set of modules we can use a software hierarchy. In this approach a user loads a compiler which extends the MODULEPATH to make available the modules that are built with the currently loaded compiler (similarly for the mpi stack).

### Task 3

#### 8. What environment variables are set by the intel and gcc modules?
The intel module sets the environment variables: CC, FC, CXX, AR, LD

The gcc module sets the environment variables: CC, FC, CXX

### Task 4

#### 9. What does ${CC} -v (where CC is either gcc or icc) print on your machine?
I installed the Intel® Parallel Studio XE for Windows. The command "icl" prints:

Intel(R) C++ Intel(R)  Compiler for applications running on Intel(R), Version 18.0.3.210 Build 20180410 Copyright (C) 1985-2018 Intel Corporation. All rights reserved.

#### 10. What are the commands for compiling the object files and linking?
If you were to use the g++ compiler:
  g++ -fopenmp -c -o axpy.o axpy.cc
  g++ -fopenmp -c -o test_axpy.o test_axpy.cc
  g++ -fopenmp axpy.o test_axpy.o  -o axpy.exe

If you were to use the intel compiler:
  icpc -fopenmp -c -o axpy.o axpy.cc
  icpc -fopenmp -c -o test_axpy.o test_axpy.cc
  icpc -fopenmp axpy.o test_axpy.o  -o axpy.exe

I put this into a make file. The command to run everything is make all.

### Task 5

#### 11. Monitor the status of the job by using commands given in the related RC guide section. What commands did you use? What are the contents of the .out and .err files. If the contents are different from what you saw interactively, debug.

To monitor the status of the job one can use the command: squeue -u $USER. This command outputs the JobID, partition the job is running on, the name of the job, user, the state the job is in (PD = pending), the number of nodes, and the node the job is running on. Running this command just after submitting the job yielded:

JOBID PARTITION NAME USER ST TIME NODES NODELIST(REASON)
1182705 shas job-axpy sabe4441 PD 0:00 1 (Priority)

The contents of the .out file (axpy-1182705.out) are:
Running on shas0507
Calling axpy - Passed
Elapsed time: 1.90735e-05s

The contents of the .err file (axpy-1182705.err) was empty.

### Task 6

#### 12. Does the behavior of compiler or program change? Address the compiler warning you receive by adding proper flag (consult the compiler man page man icc). How does the program behavior change after compilation?

The program compiled and the axpy test passed. The addition of the #pragma section, however, raised a warning on compile:
 warning #3180: unrecognized OpenMP #pragma.

This warning can be fixed by adding the -fopenmp flag when compiling and linking.

#### 13. Set OMP_NUM_THREADS to 1, 2, 4, 8, and 16 and report the results.
The overall time elapsed to run the program changed.

OMP_NUM_THREADS=1, Elapsed time: 0.866273s
OMP_NUM_THREADS=2, Elapsed time: 1.02568s
OMP_NUM_THREADS=4, Elapsed time: 0.801568s
OMP_NUM_THREADS=8, Elapsed time: 0.615916s
OMP_NUM_THREADS=16, Elapsed time: 0.578353s
