/**
 * @file
 * @author Rahimian, Abtin <arahimian@acm.org>
 * @revision $Revision: 11 $
 * @tags $Tags: tip $
 * @date $Date: Thu Jan 01 00:00:00 1970 +0000 $
 *
 * @brief
 */

#include "semaphore.h"
#include "omp-utils.h"
#include <cassert>

Sem::Sem(int val, const char *name) :
    init_(val),
    val_(val),
    name_(name)
{
    //omp_init_lock(&val_lock_);
    //omp_init_lock(&zero_lock_);
}

Sem::~Sem(){
    assert(val_==init_);
    //omp_destroy_lock(&val_lock_);
    //omp_destroy_lock(&zero_lock_);
}

void Sem::post(){
    val_++;
}

void Sem::wait(){
    while(val_ <= 0) { ; }
    val_--;
}
