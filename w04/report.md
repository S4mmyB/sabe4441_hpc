### Assignment 4 Report
#### Sam Bennetts

##### Task 2

###### 2. a) Explain why semaphore cannot be implemented by only one lock.

##### Task 3

###### 3. List the variables that are subject to race condition and describe the potential side effect of race condition.
The two fill counters, next_put and next_get, are both susceptible to improper incrementing and decrementing due to race conditions. Consider the following example in which two producers threads are trying to update the buffer at the same time. Producer thread 1 calls put() and writes a value to the ith location in the buffer. Before producer thread 1 can increment the index counter next_put, producer thread 2 makes a call to put() and writes a value to the same ith location as producer thread 1, thus overwriting the data. A similar situation can happen with the multiple consumers. Without a lock on the counter variable, two consumer threads will read the same value twice as consumer thread 1  will not have decremented the use nex_get counter before consumer thread 2 reads the data.

does this also occur for the semaphore variables?

###### 4. Try the following shell command and explain the output.
```
    $ for i in {1..500}; do ./pc.exe 10 3 3 2>/dev/null;done |sort|uniq -c
```
The code gets stuck while running this command. When I run the program once (without looping it 500x), the elements begin to print out of order. Furthermore the same 5 elements are printed a few times before the program gets stuck. This error is likely due to the race conditions that occur with the counters.

 ###### 5. Add locks to pc.cc to protect the critical sections of the code to avoid race condition. Make sure that the code works correctly after these changes.
